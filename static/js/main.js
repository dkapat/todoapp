$(document).ready(function(){
    $('.input-group.date').datepicker({
        format: 'yyyy-mm-dd',
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true
    });
});